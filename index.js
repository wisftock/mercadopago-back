require('dotenv').config();
const express = require('express');
const mercadopago = require('mercadopago');
const cors = require('cors');

const port = process.env.PORT;
const app = express();
app.use(cors());
// opera
mercadopago.configure({
  access_token: 'APP_USR-8208253118659647-112521-dd670f3fd6aa9147df51117701a2082e-677408439',
  integrator_id: 'dev_24c65fb163bf11ea96500242ac130004',
});

app.use(express.json());

app.use(express.static('public'));
// app.get('/', (req, res) => {
//   res.json('res');
// });
app.post('/create_preference', (req, res) => {
  let preference = {
    items: [
      {
        id: 1234,
        title: 'mi producto',
        description: 'este es una descripcion',
        unit_price: 5,
        quantity: 1,
        picture_url: 'https://www.mercadopago.com/org-img/MP3/home/logomp3.gif',
      },
    ],
    back_urls: {
      success: 'https://mercadopago-front.vercel.app/success',
      failure: 'https://mercadopago-front.vercel.app/pending',
      pending: 'https://mercadopago-front.vercel.app/failure',
    },
    auto_return: 'approved',
    payment_methods: {
      excluded_payment_methods: [
        {
          id: 'visa',
        },
      ],
      excluded_payment_types: [
        {
          id: 'visa',
        },
      ],
      installments: 6,
    },
    payer: {
      name: 'Lalo',
      surname: 'Landa',
      email: 'test_user_46542185@testuser.com',
      phone: {
        area_code: '11',
        number: 987987987,
      },
      address: {
        street_name: 'Street',
        street_number: 123,
        zip_code: '1234',
      },
    },
    external_reference: 'anyelosalvatierra9671@gmail.com',
    notification_url: 'https://webhook.site/5f41cdca-a1bc-4d26-982e-dcd6d1ebcc0d',
  };
  mercadopago.preferences
    .create(preference)
    .then(function (response) {
      res.json({
        id: response.body.id,
        response,
      });
    })
    .catch(function (error) {
      console.log(error);
    });
});

app.listen(port, () => {
  console.log(`http://localhost: ${port}`);
});
